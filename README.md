This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`


Runs the app.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


View Documentation.md for its CORE functionality and what works